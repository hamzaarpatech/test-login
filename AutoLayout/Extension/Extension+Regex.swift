//
//  Extension_Authentication.swift
//  AutoLayout
//
//  Created by Hamza on 1/27/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import UIKit
   /*
    Email Password Regex Validation
    sample valid pass = a#R!12
 */
   
extension String {
   var isValidEmail : Bool {
       let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"

          let emailcheck = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
          return emailcheck.evaluate(with: self)
   }
   var isValidPass : Bool {
       let passwordRegex = "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{6,}"
       let passcheck = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
       return passcheck.evaluate(with: self)
   }
}
