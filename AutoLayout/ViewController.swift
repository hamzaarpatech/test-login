//
//  ViewController.swift
//  AutoLayout
//
//  Created by Hamza on 1/27/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class ViewController: UIViewController  {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        closeKeyboard()
        setupUI()
    }
    
    func setupUI() {
        emailTextField.delegate = self
        passTextField.delegate = self
        emailTextField.tintColor = UIColor.lightGray
        emailTextField.setTextfieldImage(#imageLiteral(resourceName: "envelope (1)"))
        passTextField.tintColor = UIColor.lightGray
        passTextField.setTextfieldImage(#imageLiteral(resourceName: "password"))
        emailTextField.returnKeyType = .next
        passTextField.returnKeyType = .done
    }
    
    func closeKeyboard(){
        let gesture = UITapGestureRecognizer(target: view, action: #selector(UIView.endEditing(_:)))
        view.addGestureRecognizer(gesture)
    }
    
    /*
     OnCLick Login Button Function
     */

    @IBAction func loginButtonClicked(_ sender: Any) {
        if (emailTextField.text?.isValidEmail)! {
            print("email is valid")
        } else {
            print("not valid emaill")
        }
        
        if (passTextField.text?.isValidPass)! {
            print("valid pass")
        } else {
            print("invalid pass")
        }
    }
}

extension ViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.emailTextField {
            passTextField.becomeFirstResponder()
        } else if textField == self.passTextField {
            emailTextField.resignFirstResponder()
            loginButtonClicked(Any.self)
            closeKeyboard()
        }
        return true
    }
}
